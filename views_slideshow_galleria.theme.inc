<?php

/**
 * @file
 * Theme & preprocess functions for the Views Slideshow: Galleria.
 */

use Drupal\Component\Utility\Html;

/**
 * Views Slideshow: Theme the main frame wrapper.
 *
 * @ingroup vss_theme
 */
function template_preprocess_views_slideshow_galleria_main_frame(&$vars) {
  if ($vars['settings']['advanced']['strip_images']) {
    // Initialize our $images array.
    $vars['images'] = [];

    $vars['original_rows'] = $vars['rows'];
    // Strip all images from the $rows created by the original view query.
    foreach ($vars['rows'] as $id => $item) {
      $item_rendered = \Drupal::service('renderer')->render($item);
      preg_match('@(<\s*img\s+[^>]*>)@i', $item_rendered, $matches);
      if (@$image = $matches[1]) {
        // If our image is in an anchor tag, use its URL.
        preg_match('@<\s*a\s+href\s*=\s*"\s*([^"]+)\s*"[^>]*>[^<]*' . preg_quote($image) . '[^<]*<\s*/a\s*>@i', $item_rendered, $urls);
        if (isset($urls[1])) {
          $url = $urls[1];
        }
        else {
          // Otherwise link to the original image or the front page instead.
          preg_match('@src\s*=\s*"([^"]+)"@i', $image, $urls);
          $url = isset($urls[1]) ? $urls[1] : url('<front>');
        }

        // Ensure the link for the original image is preserved.
        // $url has already been url'ized.
        $vars['rows'][$id] = '<a href="' . $url . '">' . $image . '</a>';

        // Add the image to our image array to display.
        $vars['images'][$id] = $image;
      }
    }
  }

  _views_slideshow_galleria_add_js($vars['settings'], $vars);

  $vars['attributes']['class'][] = 'views-slideshow-galleria-images';
  $vars['attributes']['class'][] = 'galleria';
  if ($vars['settings']['advanced']['avoid_flash_of_content']) {
    $vars['attributes']['class'][] = 'views-slideshow-galleria-hidden';
  }

  $vars['#attached']['library'][] = 'views_slideshow_galleria/views_slideshow_galleria';
}

/**
 * Helper function to build and pass Galleria settings to Drupal.
 */
function _views_slideshow_galleria_add_js($options, &$vars) {
  static $loaded_themes;
  global $base_url;

  // Keep track of which Galleria plugin JS themes have already been loaded.
  if (!isset($loaded_themes)) {
    $loaded_themes = [];
  }

  // Import variables.
  extract($options);
  extract($advanced);
  extract($custom_theme_options);

  // Load our galleria js.
  $vars['#attached']['library'][] = 'views_slideshow_galleria/galleria.js';

  if ($history) {
    $vars['#attached']['library'][] = 'views_slideshow_galleria/galleria.plugins.history';
  }

  // Process Galleria settings.
  $settings = [
    'autoplay' => ($autoplay) ? (($autoplay_ms) ? (int) $autoplay_ms : TRUE) : FALSE,
    'carousel' => (bool) $carousel,
    'carouselFollow' => (bool) $carousel_follow,
    'carouselSpeed' => (int) Html::escape($carousel_speed),
    'carouselSteps' => ($carousel_steps == 'auto') ? $carousel_steps : (int) $carousel_steps,
    'clicknext' => (bool) $clicknext,
    'debug' => (bool) $debug,
    'dummy' => Html::escape($dummy),
    'easing' => Html::escape($easing),
    'fullscreenCrop' => (bool) $fullscreen_crop,
    'fullscreenDoubleTap' => (bool) $fullscreen_double_tap,
    'fullscreenTransition' => (bool) $fullscreen_double_tap,
    'height' => (float) Html::escape($height),
    'idleMode' => (bool) $idle_mode,
    'idleTime' => (int) Html::escape($idle_time),
    'imageCrop' => ($image_crop == 'width' || $image_crop == 'height') ? $image_crop : (bool) $image_crop,
    'imageMargin' => (int) Html::escape($image_margin),
    'imagePan' => (bool) $image_pan,
    'imagePanSmoothness' => (int) $image_pan_smoothness,
    'imagePosition' => Html::escape($image_position),
    'keepSource' => (bool) $keep_source,
    'layerFollow' => (bool) $layer_follow,
    'lightbox' => (bool) $lightbox,
    'lightboxFadeSpeed' => (int) Html::escape($lightbox_fade_speed),
    'lightboxTransitionSpeed' => (int) Html::escape($lightbox_transition_speed),
    'maxScaleRatio' => (int) $max_scale_ratio,
    'minScaleRatio' => (int) $min_scale_ratio,
    'overlayOpacity' => (float) $overlay_opacity,
    'overlayBackground' => Html::escape($overlay_background),
    'pauseOnInteraction' => (bool) $pause_on_interaction,
    'popupLlinks' => (bool) $popup_links,
    'preload' => ($preload == 'all') ? 'all' : (int) $preload,
    'queue' => (bool) $queue,
    'responsive' => (bool) $responsive,
    'show' => (int) $show,
    'showInfo' => (bool) $show_info,
    'showCounter' => (bool) $show_counter,
    'showImagenav' => (bool) $show_imagenav,
    'swipe' => (bool) $swipe,
    'thumbCrop' => ($thumb_crop == 'width' || $thumb_crop == 'height') ? $thumb_crop : (bool) $thumb_crop,
    'thumbFit' => (bool) $thumb_fit,
    'thumbMargin' => (int) Html::escape($thumb_margin),
    'thumbQuality' => ($thumb_quality == 'auto') ? 'auto' : (bool) $thumb_quality,
    'thumbnails' => ($thumbnails == 'empty' || $thumbnails == 'numbers') ? $thumbnails : (bool) $thumbnails,
    'touchTransition' => Html::escape($touch_transition),
    'transition' => Html::escape($transition),
    'transitionSpeed' => (int) $transition_speed,
    'width' => ($width == 'auto') ? $width : (int) Html::escape($width),
  ];

  if ($theme == 'custom' && !empty($custom_theme) && !empty($custom_theme_path)) {
    $theme = Html::escape($custom_theme);
    $theme_path = $base_url . $custom_theme_path;
  }
  else {
    $theme_path = "{$base_url}/libraries/galleria/themes/{$theme}/galleria.{$theme}.min.js";
  }

  // Load the Galleria theme.
  if (!isset($loaded_themes[$theme_path])) {
    $settings['themePath'] = $theme_path;
    $loaded_themes[$theme_path] = TRUE;
  }

  // Process advanced settings.
  if (isset($extend) && !empty($extend)) {
    $settings['extend'] = $extend;
  }
  if (isset($data_config) && !empty($data_config)) {
    $settings['dataConfig'] = $data_config;
  }
  if (isset($data_source) && !empty($data_source)) {
    $settings['dataSource'] = $data_source;
  }

  // Load galleria settings.
  $vars['#attached']['drupalSettings']['viewsSlideshowGalleria']['views-slideshow-galleria-images-' . $vars['vss_id']] = $settings;
}
