
 Views Slideshow: Galleria
----------------------------

Integrating Galleria with Views Slideshow.

This module will display a view of images using the Galleria jQuery plugin
available from http://galleria.io.

Galleria
— A JavaScript gallery for the Fastidious

Galleria is a JavaScript image gallery unlike anything else. It can take a
simple list of images and turn it into a foundation of multiple intelligent
gallery designs, suitable for any project.

Dependencies
------------

1.[Views Slideshow](https://www.drupal.org/project/views_slideshow)

2. [Galleria.io](https://galleria.io/) library file



Installation
------------

1. Downloading module.

a) Download using composer.

`composer require drupal/views_slideshow_galleria`

This will download views_slideshow_galleria module and also views_slideshow
module.

b) Manual download.

Place the manually downloaded module to modules directory. Also make sure
views_slideshow module is also downloaded and placed inside modules directory.

c) Download using drush.

`drush dl views_slideshow_galleria`

This will download views_slideshow_galleria module and also views_slideshow
module.

d) Download using drupal console.

`drupal mod views_slideshow_galleria`

This will download views_slideshow_galleria module and also views_slideshow
module.

2. Download Galleria.io file.

Download and unzip Galleria.io library file and place it inside libraries folder
of drupal root. [Download Link](https://galleria.io/static/galleria-1.5.7.zip)

3. Install views_slideshow and views_slideshow_galleria module

4. You should now see the new views style option called "Slideshow"

Authors/maintainers
-------------------

Original Author:

Aaron Winborn (winborn at advomatic dot com)
https://drupal.org/user/33420

-maintainers:

NickWilde
https://www.drupal.org/u/nickwilde

redndahead
https://drupal.org/user/160320

psynaptic
https://drupal.org/user/93429
