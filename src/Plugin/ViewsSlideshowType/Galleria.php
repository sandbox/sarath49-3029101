<?php

namespace Drupal\views_slideshow_galleria\Plugin\ViewsSlideshowType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\views_slideshow\ViewsSlideshowTypeBase;
use Drupal\Core\Link;

/**
 * Provides a slideshow type based on galleria.
 *
 * @ViewsSlideshowType(
 *   id = "views_slideshow_galleria",
 *   label = @Translation("Galleria"),
 *   accepts = {
 *     "goToSlide",
 *     "nextSlide",
 *     "previousSlide"
 *   },
 *   calls = {
 *     "goToSlide",
 *     "nextSlide",
 *     "previousSlide"
 *   }
 * )
 */
class Galleria extends ViewsSlideshowTypeBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'contains' => [
        'theme' => ['default' => 'classic'],
        'autoplay' => ['default' => FALSE],
        'autoplay_ms' => ['default' => 3000],
        'carousel' => ['default' => TRUE],
        'carousel_follow' => ['default' => TRUE],
        'carousel_speed' => ['default' => 200],
        'carousel_steps' => ['default' => 'auto'],
        'clicknext' => ['default' => FALSE],
        'dummy' => ['default' => ''],
        'custom_theme_options' => [
          'contains' => [
            'custom_theme' => ['default' => ''],
            'custom_theme_path' => ['default' => ''],
          ],
        ],
        'easing' => ['default' => 'galleria'],
        'fullscreen_crop' => ['default' => ''],
        'fullscreen_double_tap' => ['default' => TRUE],
        'fullscreen_transition' => ['default' => ''],
        'height' => ['default' => 0],
        'idle_mode' => ['default' => TRUE],
        'idle_time' => ['default' => 3000],
        'idle_speed' => ['default' => 200],
        'image_crop' => ['default' => FALSE],
        'image_margin' => 0,
        'image_pan' => ['default' => FALSE],
        'image_pan_smoothness' => ['default' => 12],
        'image_position' => ['default' => 'center'],
        'initial_transition' => ['default' => ''],
        'layer_follow' => ['default' => TRUE],
        'lightbox' => ['default' => FALSE],
        'lightbox_fade_speed' => ['default' => 200],
        'lightbox_transition_speed' => ['default' => 300],
        'max_scale_ratio' => ['default' => ''],
        'min_scale_ratio' => ['default' => ''],
        'overlay_opacity' => ['default' => 0.85],
        'overlay_background' => ['default' => '#0b0b0b'],
        'pause_on_interaction' => ['default' => TRUE],
        'popup_links' => ['default' => FALSE],
        'preload' => ['default' => 2],
        'queue' => ['default' => TRUE],
        'responsive' => ['default' => FALSE],
        'show' => ['default' => 0],
        'show_info' => ['default' => TRUE],
        'show_counter' => ['default' => TRUE],
        'show_imagenav' => ['default' => TRUE],
        'swipe' => ['default' => TRUE],
        'theme' => ['default' => 'classic'],
        'thumb_crop' => ['default' => FALSE],
        'thumb_fit' => ['default' => TRUE],
        'thumb_margin' => ['default' => 0],
        'thumb_quality' => ['default' => TRUE],
        'thumbnails' => ['default' => TRUE],
        'touch_transition' => ['default' => ''],
        'transition' => ['default' => 'fade'],
        'transition_speed' => ['default' => 400],
        'width' => ['default' => 'auto'],
        'advanced' => [
          'contains' => [
            'avoid_flash_of_content' => ['default' => TRUE],
            'data_config' => ['default' => ''],
            'data_selector' => ['default' => 'img'],
            'data_source' => ['default' => ''],
            'debug' => ['default' => TRUE],
            'extend' => ['default' => ''],
            'history' => ['default' => 0],
            'keep_source' => ['default' => FALSE],
            'strip_images' => ['default' => TRUE],
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $galleria = \Drupal::service('library.discovery')->getLibraryByName('views_slideshow_galleria', 'galleria.js');
    if (!isset($galleria['js'][0]['data']) || !file_exists($galleria['js'][0]['data'])) {
      $form['views_slideshow_galleria']['no_galleria_js'] = [
        '#markup' => '<div style="color: red">' . $this->t('You need to install the Galleria plugin. Create a directory in libraries (which should be in your Drupal root folder, if not create the same) called galleria. You can find the plugin at @url.',
                [
                  '@url' => Link::fromTextAndUrl('http://galleria.io', Url::FromUri('http://galleria.io', ['attributes' => ['target' => '_blank']]))->toString(),
                ]) . '</div>',
      ];
    }

    $transition_options = [
      'fade' => $this->t('Fade'),
      'flash' => $this->t('Flash'),
      'pulse' => $this->t('Pulse'),
      'slide' => $this->t('Slide'),
      'fadeslide' => $this->t('Fade/Slide'),
    ];

    $crop_options = [
      'width' => $this->t('Width'),
      'height' => $this->t('Height'),
      0 => $this->t('False'),
      1 => $this->t('True'),
    ];

    $form['views_slideshow_galleria']['theme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Galleria theme to load on display. If you choose the %custom option, you must specify the path to your custom JavaScript theme below.',
            [
              '%custom' => $this->t('<Custom>'),
            ]
      ),
      '#default_value' => $this->getConfiguration()['theme'],
      '#options' => [
        'custom' => $this->t('Custom (specify options below)'),
        'miniml' => $this->t('Miniml'),
        'twelve' => $this->t('Twelve'),
        'fullscreen' => $this->t('Fullscreen'),
        'classic' => $this->t('Classic'),
        'folio' => $this->t('Folio'),
        'azur' => $this->t('Azur'),
      ],
    ];
    $form['views_slideshow_galleria']['custom_theme_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Custom theme options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['views_slideshow_galleria']['custom_theme_options']['custom_theme'] = [
      '#type' => 'textfield',
      '#title' => t('Custom theme'),
      '#description' => $this->t('Leave this blank unless you wish to override the theme used by the plugin. You should specify the name of the custom theme here, and enter its path below. See the @theme_api for how to create your own Galleria JavaScript themes.',
            [
              '@theme_api' => Link::fromTextAndUrl($this->t('Galleria JavaScript theme API'), Url::FromUri('https://docs.galleria.io/article/39-creating-a-theme', ['attributes' => ['target' => '_blank']]))->toString(),
              '%custom' => $this->t('<Custom>'),
            ]
      ),
      '#default_value' => $this->getConfiguration()['custom_theme_options']['custom_theme'],
    ];
    $form['views_slideshow_galleria']['custom_theme_options']['custom_theme_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom theme path'),
      '#description' => $this->t("Leave this blank unless you're overriding the theme. See the @theme_api for how to create your own Galleria JavaScript themes.",
            [
              '@theme_api' => Link::fromTextAndUrl($this->t('Galleria JavaScript theme API'), Url::FromUri('https://docs.galleria.io/article/39-creating-a-theme', ['attributes' => ['target' => '_blank']])),
            ]
      ),
      '#default_value' => $this->getConfiguration()['custom_theme_options']['custom_theme_path'],
    ];

    $form['views_slideshow_galleria']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => t('Autoplay'),
      '#description' => $this->t('If checked, then the slide show will begin rotating after the specified time below.'),
      '#default_value' => $this->getConfiguration()['autoplay'],
    ];

    $form['views_slideshow_galleria']['autoplay_ms'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Autoplay time'),
      '#description' => $this->t('If the slide show is set to autoplay above, then begin after this many miliseconds.'),
      '#default_value' => $this->getConfiguration()['autoplay_ms'],
    ];
    $form['views_slideshow_galleria']['carousel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Carousel'),
      '#description' => $this->t('If checked, this will activate the carousel when needed. Otherwise it will not appear at all.'),
      '#default_value' => $this->getConfiguration()['carousel'],
    ];
    $form['views_slideshow_galleria']['carousel_follow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Carousel follow'),
      '#description' => $this->t('If checked, the carousel will follow the active image.'),
      '#default_value' => $this->getConfiguration()['carousel_follow'],
    ];
    $form['views_slideshow_galleria']['carousel_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carousel speed'),
      '#description' => $this->t('The slide speed of the carousel in milliseconds.'),
      '#default_value' => $this->getConfiguration()['carousel_speed'],
    ];
    $form['views_slideshow_galleria']['carousel_steps'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Carousel steps'),
      '#description' => $this->t('The number of "steps" the carousel will slide when navigating between available thumbnails. Specifying %auto will move the carousel as many steps as there are visible thumbnails.', ['%auto' => 'auto']),
      '#default_value' => $this->getConfiguration()['carousel_steps'],
    ];
    $form['views_slideshow_galleria']['clicknext'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Click next'),
      '#description' => $this->t('This options adds a click event over the stage that navigates to the next image in the gallery. Useful for mobile browsers and other simpler applications.'),
      '#default_value' => $this->getConfiguration()['clicknext'],
    ];
    $form['views_slideshow_galleria']['dummy'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Dummy'),
      '#description' => $this->t('This option allows you to define an image that should be shown if Galleria can’t find the original image.'),
      '#default_value' => $this->getConfiguration()['dummy'],
    ];
    $form['views_slideshow_galleria']['easing'] = [
      '#type' => 'select',
      '#title' => $this->t('Easing'),
      '#description' => $this->t('You can use this option to control the animation easing on a global level in Galleria.'),
      '#default_value' => $this->getConfiguration()['easing'],
      '#options' => [
        'galleria' => 'galleria',
        'galleriain' => 'galleriaIn',
        'galleriaout' => 'galleriaOut',
      ],
    ];
    $form['views_slideshow_galleria']['fullscreen_crop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fullscreen crop'),
      '#description' => $this->t('Sets how Galleria should crop when in fullscreen mode. See imageCrop for cropping options.'),
      '#default_value' => $this->getConfiguration()['fullscreen_crop'],
      '#options' => $crop_options,
    ];
    $form['views_slideshow_galleria']['fullscreen_double_tap'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fullscreen double tap'),
      '#description' => $this->t('This options listens for the double-tap event on touch devices and toggle fullscreen mode if it happens.'),
      '#default_value' => $this->getConfiguration()['fullscreen_double_tap'],
    ];
    $form['views_slideshow_galleria']['fullscreen_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Fullscreen transition'),
      '#description' => $this->t('Defines a different transition for fullscreen mode. Some transitions are less smooth in fullscreen mode, this option allows you to set a different transition when in fullscreen mode.'),
      '#default_value' => $this->getConfiguration()['fullscreen_transition'],
      '#options' => $transition_options,
    ];
    $form['views_slideshow_galleria']['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#description' => $this->t('This will set a height to the gallery. If you set this to %auto and no CSS height is found, Galleria will automatically add a 16/9 ratio as a fallback.', ['%auto' => 'auto']),
      '#default_value' => $this->getConfiguration()['height'],
      '#required' => TRUE,
    ];
    $form['views_slideshow_galleria']['idle_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Idle mode'),
      '#description' => $this->t('Global option for turning on/off idle mode. Eache gallery enters idle mode after certain amount of time and themes behave differently when this happens, f.ex clears the stage from distractions.'),
      '#default_value' => $this->getConfiguration()['idle_mode'],
    ];
    $form['views_slideshow_galleria']['idle_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Idle time'),
      '#description' => $this->t('If you are adding elements into idle mode using the .addIdleState() method, you can control the delay before Galleria falls into Idle mode using this option.'),
      '#default_value' => $this->getConfiguration()['idle_time'],
    ];
    $form['views_slideshow_galleria']['idle_speed'] = [
      '#type' => 'textfield',
      '#title' => t('Idle speed'),
      '#description' => t('If you are adding elements into idle mode using the addIdleState() method, you can control the animation speed of the idle elements.'),
      '#default_value' => $this->getConfiguration()['idle_speed'],
    ];
    $form['views_slideshow_galleria']['image_crop'] = [
      '#type' => 'select',
      '#title' => $this->t('Image crop'),
      '#description' => $this->t('Defines how the main image will be cropped inside it’s container.'),
      '#default_value' => $this->getConfiguration()['image_crop'],
      '#options' => $crop_options,
    ];
    $form['views_slideshow_galleria']['image_margin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image margin'),
      '#description' => $this->t('Sets a margin between the image and stage. Specify the number of pixels.'),
      '#default_value' => $this->getConfiguration()['image_margin'],
    ];
    $form['views_slideshow_galleria']['image_pan'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Image pan'),
      '#description' => $this->t('Galleria comes with a built-in panning effect. The effect is sometimes useful if you have cropped images and want to let the users pan across the stage to see the entire image.'),
      '#default_value' => $this->getConfiguration()['image_pan'],
    ];
    $form['views_slideshow_galleria']['image_pan_smoothness'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image pan smoothness'),
      '#description' => $this->t('This value sets how “smooth” the image pan movement should be when setting image_pan to true. The higher value, the smoother effect but also CPU consuming.'),
      '#default_value' => $this->getConfiguration()['image_pan_smoothness'],
    ];
    $form['views_slideshow_galleria']['image_position'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image position'),
      '#description' => $this->t("Positions the main image. Works like the CSS background-position property; i.e., 'top right' or '20% 100%'. You can use keywords, percents or pixels. The first value is the horizontal position and the second is the vertical. Read more at :read_more.",
            [
              ':read_more' => Link::fromTextAndUrl('http://www.w3.org/TR/REC-CSS1/#background-position', Url::FromUri('http://www.w3.org/TR/REC-CSS1/#background-position'),
                    ['attributes' => ['target' => '_blank']]
              ),
            ]
      ),
      '#default_value' => $this->getConfiguration()['image_position'],
    ];
    $form['views_slideshow_galleria']['initial_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Initial transition'),
      '#description' => $this->t('Defines a different transition to show on the first image.'),
      '#default_value' => $this->getConfiguration()['initial_transition'],
      '#options' => $transition_options,
    ];
    $form['views_slideshow_galleria']['layer_follow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Layer follow'),
      '#description' => $this->t('If checked, the source HTML will be left intact, which will also create clickable images of each image inside the source. Useful for building custom thumbnails and still have galleria control the gallery.'),
      '#default_value' => $this->getConfiguration()['layer_follow'],
    ];
    $form['views_slideshow_galleria']['lightbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lightbox'),
      '#description' => $this->t('This option acts as a helper for attaching a lightbox when the user clicks on an image. If you have a link defined for the image, the link will take precedence.'),
      '#default_value' => $this->getConfiguration()['lightbox'],
    ];
    $form['views_slideshow_galleria']['lightbox_fade_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lightbox fade speed'),
      '#description' => $this->t('When calling .showLightbox() the lightbox will animate and fade the images and captions. This value controls how fast they should fade in milliseconds.'),
      '#default_value' => $this->getConfiguration()['lightbox_fade_speed'],
    ];
    $form['views_slideshow_galleria']['lightbox_transition_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lightbox transition speed'),
      '#description' => $this->t('When calling .showLightbox() the lightbox will animate the white square before displaying the image. This value controls how fast it should animate in milliseconds.'),
      '#default_value' => $this->getConfiguration()['lightbox_transition_speed'],
    ];
    $form['views_slideshow_galleria']['max_scale_ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max scale ratio'),
      '#description' => $this->t("Sets the maximum scale ratio for images. If you don't want Galleria to upscale any images, set this to 1. Leaving it blank will allow any scaling of the images."),
      '#default_value' => $this->getConfiguration()['max_scale_ratio'],
    ];
    $form['views_slideshow_galleria']['min_scale_ratio'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Min scale ratio'),
      '#description' => $this->t("Sets the minimum scale ratio for images. F.ex, if you don’t want Galleria to downscale any images, set this to 1."),
      '#default_value' => $this->getConfiguration()['min_scale_ratio'],
    ];
    $form['views_slideshow_galleria']['overlay_opacity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overlay opacity'),
      '#description' => $this->t('This sets how much opacity the overlay should have when calling showLightbox().'),
      '#default_value' => $this->getConfiguration()['overlay_opacity'],
    ];
    $form['views_slideshow_galleria']['overlay_background'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overlay background'),
      '#description' => $this->t('This defines the overlay background color when calling showLightbox().'),
      '#default_value' => $this->getConfiguration()['overlay_background'],
    ];
    $form['views_slideshow_galleria']['pause_on_interaction'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pause on interaction'),
      '#description' => $this->t('During playback, Galleria will stop the playback if the user presses thumbnails or any other navigational links.'),
      '#default_value' => $this->getConfiguration()['pause_on_interaction'],
    ];
    $form['views_slideshow_galleria']['popup_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Popup links'),
      '#description' => $this->t('Checking this box will open any image links in a new window.'),
      '#default_value' => $this->getConfiguration()['popup_links'],
    ];
    $form['views_slideshow_galleria']['preload'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preload'),
      '#description' => $this->t("Defines how many images Galleria should preload in advance. Please note that this only applies when you are using separate thumbnail files. Galleria always cache all preloaded images. <ul><li>%2 preloads the next 2 images in line.</li><li>%all forces Galleria to start preloading all images. This may slow down client.</li><li>%0 will not preload any images</li></ul>",
          [
            '%2' => '2',
            '%all' => 'all',
            '%0' => '0',
          ]
      ),
      '#default_value' => $this->getConfiguration()['preload'],
    ];
    $form['views_slideshow_galleria']['queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Queue'),
      '#description' => $this->t("Galleria queues all activation clicks (next/prev & thumbnails). You can see this effect when, for example, clicking %next many times. If you don't want Galleria to queue, then uncheck the box.", ['%next' => t('next')]),
      '#default_value' => $this->getConfiguration()['queue'],
    ];
    $form['views_slideshow_galleria']['responsive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Responsive'),
      '#description' => $this->t("Sets Galleria in responsive mode."),
      '#default_value' => $this->getConfiguration()['responsive'],
    ];
    $form['views_slideshow_galleria']['show'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Show'),
      '#description' => $this->t("This defines what image index to show at first. If you have left the %history box checked, then a permalink will override this number.", ['%history' => t('History permalinks')]),
      '#default_value' => $this->getConfiguration()['show'],
    ];
    $form['views_slideshow_galleria']['show_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show info'),
      '#description' => $this->t('Set this to false if you do not wish to display the caption.'),
      '#default_value' => $this->getConfiguration()['show_info'],
    ];
    $form['views_slideshow_galleria']['show_counter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show counter'),
      '#description' => $this->t('Set this to false if you do not wish to display the counter.'),
      '#default_value' => $this->getConfiguration()['show_counter'],
    ];
    $form['views_slideshow_galleria']['show_imagenav'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show image navigation'),
      '#description' => $this->t('Set this to false if you do not wish to display the image navigation (next/prev arrows).'),
      '#default_value' => $this->getConfiguration()['show_imagenav'],
    ];
    $form['views_slideshow_galleria']['swipe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Swipe'),
      '#description' => $this->t('Enables a swipe movement for flicking through images on touch devices.'),
      '#default_value' => $this->getConfiguration()['swipe'],
    ];
    $form['views_slideshow_galleria']['thumb_crop'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumb crop'),
      '#description' => $this->t('Same as imageCrop, but for thumbnails.'),
      '#default_value' => $this->getConfiguration()['thumb_crop'],
      '#options' => $crop_options,
    ];
    $form['views_slideshow_galleria']['thumb_fit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Thumb fit'),
      '#description' => $this->t("If this is set to ‘true’, all thumbnail containers will be shrinked to fit the actual thumbnail size. This is useful if you have thumbnails of various sizes and will then float nicely side-by-side. This is only relevant if thumbCrop is set to anything else but ‘true’. If you want all thumbnails to fit inside a container with predefined width & height, set this to ‘false’."),
      '#default_value' => $this->getConfiguration()['thumb_fit'],
    ];
    $form['views_slideshow_galleria']['thumb_margin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thumb margin'),
      '#description' => $this->t('Same as %imagemargin, but for thumbnails.', ['%imagemargin' => $this->t('Image margin')]),
      '#default_value' => $this->getConfiguration()['thumb_margin'],
    ];
    $form['views_slideshow_galleria']['thumb_quality'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumb quality'),
      '#description' => $this->t("Defines if and how IE should use bicubic image rendering for thumbnails.<ul><li>%auto uses high quality if image scaling is moderate.</li><li>%false will not use high quality (better performance).</li><li>%true will force high quality renedring (can slow down performance)</li></ul>",
          [
            '%auto' => t('Auto'),
            '%false' => t('False'),
            '%true' => t('True'),
          ]
      ),
      '#default_value' => $this->getConfiguration()['thumb_quality'],
      '#options' => [
        'auto' => $this->t('Auto'),
        0 => $this->t('False'),
        1 => $this->t('True'),
      ],
    ];
    $form['views_slideshow_galleria']['thumbnails'] = [
      '#type' => 'select',
      '#title' => $this->t('Thumbnails'),
      '#description' => $this->t("Sets the creation of thumbnails. If set to %false, then Galleria will not create thumbnails. If you set this to %empty, Galleria will create empty spans with the className %img instead of thumbnails.",
            [
              '%empty' => $this->t('Empty'),
              '%image' => $this->t('img'),
              '%false' => $this->t('False'),
            ]
      ),
      '#default_value' => $this->getConfiguration()['thumbnails'],
      '#options' => [
        'empty' => $this->t('Empty'),
        'numbers' => $this->t('Numbers'),
        0 => $this->t('False'),
        1 => $this->t('True'),
      ],
    ];
    $form['views_slideshow_galleria']['touch_transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Touch transition'),
      '#description' => $this->t('Defines a different transition when a touch device is detected.'),
      '#default_value' => $this->getConfiguration()['touch_transition'],
      '#options' => $transition_options,
    ];
    $form['views_slideshow_galleria']['transition'] = [
      '#type' => 'select',
      '#title' => $this->t('Transition'),
      '#description' => $this->t("The transition that is used when displaying the images.<ul><li>%fade will fade between images.</li><li>%flash will fade into the background color between images.</li><li>%slide will slide the images using the Galleria easing depending on image position.</li><li>%fadeslide will fade between images and slide slightly at the same time.</li></ul>",
            [
              '%fade' => $this->t('Fade'),
              '%flash' => $this->t('Flash'),
              '%slide' => $this->t('Slide'),
              '%fadeslide' => $this->t('Fade/Slide'),
            ]
      ),
      '#default_value' => $this->getConfiguration()['transition'],
      '#options' => $transition_options,
    ];
    $form['views_slideshow_galleria']['transition_speed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Transition speed'),
      '#description' => $this->t("The milliseconds used when applying the transition."),
      '#default_value' => $this->getConfiguration()['transition_speed'],
    ];
    $form['views_slideshow_galleria']['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#description' => $this->t('This will set a width to the gallery.'),
      '#default_value' => $this->getConfiguration()['width'],
      '#required' => TRUE,
    ];
    $form['views_slideshow_galleria']['advanced'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Advanced settings'),
      '#description' => $this->t('WARNING: Some of these settings will pass raw JavaScript to the browser, so should be used with care. See the :docs for more information on their usage.',
            [
              ':docs' => Link::fromTextAndUrl($this->t('documentation'), Url::fromUri('https://docs.galleria.io/collection/25-options', ['attributes' => ['target' => '_blank']])),
            ]
      ),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['views_slideshow_galleria']['history'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('History permalinks'),
      '#description' => $this->t('Add permalinks to all images in the gallery.'),
      '#default_value' => $this->getConfiguration()['history'],
    ];

    $form['views_slideshow_galleria']['advanced']['data_config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data config'),
      '#description' => $this->t('This javascript function configures how the data should be extracted from the source. It should return an object that will blend in with the default extractions. WARNING: Raw JavaScript will be passed here.'),
      '#default_value' => $this->getConfiguration()['advanced']['data_config'],
    ];
    $form['views_slideshow_galleria']['advanced']['data_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data image selector'),
      '#description' => $this->t('The selector Galleria should look for in the HTML source. Defaults to %img and there is rarely any reason to change this.', ['%img' => 'img']),
      '#default_value' => $this->getConfiguration()['advanced']['data_selector'],
    ];
    $form['views_slideshow_galleria']['advanced']['data_source'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Data source'),
      '#description' => $this->t('This is where Galleria finds the data depending on data_type. It defaults to the target selector, which is the same element that was used in the jQuery plugin.'),
      '#default_value' => $this->getConfiguration()['advanced']['data_source'],
    ];
    $form['views_slideshow_galleria']['advanced']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('If checked, the slide show will throw errors when something is not right.'),
      '#default_value' => $this->getConfiguration()['advanced']['debug'],
    ];
    $form['views_slideshow_galleria']['advanced']['extend'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extend'),
      '#description' => $this->t("This function is used to extend the init function of the theme. Use this to quickly add minor customizations to the theme. The first argument is the options object, and the scope is always the Galleria gallery, just like the theme's init() function. WARNING: Raw JavaScript will be passed here."),
      '#default_value' => $this->getConfiguration()['advanced']['extend'],
    ];
    $form['views_slideshow_galleria']['advanced']['keep_source'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Keep source'),
      '#description' => $this->t('If checked, the source HTML will be left intact, which will also create clickable images of each image inside the source. Useful for building custom thumbnails and still have galleria control the gallery.'),
      '#default_value' => $this->getConfiguration()['advanced']['keep_source'],
    ];
    $form['views_slideshow_galleria']['advanced']['avoid_flash_of_content'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Avoid flash of content'),
      '#description' => $this->t('If checked, then the images of the Galleria slide show will be hidden by JavaScript in the page header, so that there is no flash of content on the page load.'),
      '#default_value' => $this->getConfiguration()['avoid_flash_of_content'],
    ];
    $form['views_slideshow_galleria']['advanced']['strip_images'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strip images'),
      '#description' => $this->t('If checked, then the images will be stripped from the original view query. Disabling it can be useful to add rich HTML captions.'),
      '#default_value' => $this->getConfiguration()['advanced']['strip_images'],
    ];

    return $form;
  }

}
